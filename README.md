*UPDATE*

I am doing a major overhaul on the catalog for my games.  No. 1 through 3 will be somewhat updated with ScottKit (source code is 
in the repo as *.SCK), even though ScottKit games usually compile to SAO, I compile it to DAT, which keeps it to the Scott 
Adams/Brian Howarth tradition of the data files.  So what is to be expected?  The Mystery of Lovecraft Manor, The Pirates of R'Lyeh
Island and Weekend At Wilbur's, are all going to be slightly updated (if necessary) in the source code to fix unnecessary devices.

Even though I liked the plot of No. 4, Eldritch Ghost Ship, however the game play I did not.  SO it looks like a total rewrite of 
the game is in play, that and also it is to be in another part of the catalog, maybe No. 5 or 6, due I want room for growth, so I 
am doing a sword and sorcery themed text adventure.  Enough of my spiel, I hope you enjoy what my gifts to you, fair adventurer!

-JLH